package ru.t1.zkovalenko.tm.command.task;

import org.jetbrains.annotations.NotNull;

public class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all Tasks";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final String userId = getUserId();
        getTaskService().clear(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
