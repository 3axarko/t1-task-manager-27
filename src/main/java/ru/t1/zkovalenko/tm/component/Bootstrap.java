package ru.t1.zkovalenko.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.zkovalenko.tm.api.repository.ICommandRepository;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.*;
import ru.t1.zkovalenko.tm.command.AbstractCommand;
import ru.t1.zkovalenko.tm.command.data.AbstractDataCommand;
import ru.t1.zkovalenko.tm.command.data.DataBase64LoadCommand;
import ru.t1.zkovalenko.tm.command.data.DataBinaryLoadCommand;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.exception.system.CommandNotSupportedException;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.repository.CommandRepository;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.TaskRepository;
import ru.t1.zkovalenko.tm.repository.UserRepository;
import ru.t1.zkovalenko.tm.service.*;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.SystemUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.zkovalenko.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null) throw new CommandNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new CommandNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

    public void run(@Nullable String[] args) {
        if (processArguments(args)) exit();

        initPID();
        initDemoData();
        initLogger();
        initData();
        runAutoCommands();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter Command:");
                @NotNull final String command = TerminalUtil.nextLine();
                System.out.println("---");
                processCommand(command);
                ColorizeConsoleTextUtil.greenText("[OK]");
                loggerService.command(command);
            } catch (@Nullable final Exception e) {
                ColorizeConsoleTextUtil.redText("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void initLogger() {
        loggerService.info("** Welcome to Task-Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** Task-Manager is shutting down **");
            }
        });
    }

    private void initDemoData() {
        final User firstUser = userService.create("1", "1", "12@mail.ru");
        userService.create("2", "2", "13@mail.ru");
        final User adminUser = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(adminUser.getId(), new Project("project1", Status.IN_PROGRESS));
        projectService.add(adminUser.getId(), new Project("project3", Status.NOT_STARTED));
        projectService.add(adminUser.getId(), new Project("project2", Status.IN_PROGRESS));
        projectService.add(adminUser.getId(), new Project("project4", Status.COMPLETED));

        taskService.create(adminUser.getId(), "task1");
        taskService.create(adminUser.getId(), "task2");
    }

    private void initData() {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FINAL_BINARY));
        if (checkBinary) {
            processCommand(DataBinaryLoadCommand.NAME, false);
            return;
        }
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) processCommand(DataBase64LoadCommand.NAME, false);
    }

    private void runAutoCommands() {
        getAuthService().login("admin", "admin");
    }

    private void processCommand(@Nullable final String command, boolean checkRoles) {
        if (command == null) throw new CommandNotSupportedException();
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) abstractCommand = commandService.getCommandByArgument(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

}
