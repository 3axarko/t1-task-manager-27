package ru.t1.zkovalenko.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;

public interface ColorizeConsoleTextUtil {

    @NotNull
    static String colorText(@NotNull String inText, @NotNull String color) {
        return color + inText + ConsoleColorConst.RESET;
    }

    static void colorPrintln(@NotNull String inText, @NotNull String color) {
        System.out.println(colorText(inText, color));
    }

    static void greenText(@NotNull String string) {
        colorPrintln(string, ConsoleColorConst.GREEN);
    }

    static void redText(@NotNull String string) {
        colorPrintln(string, ConsoleColorConst.RED);
    }

}
